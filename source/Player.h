#pragma once
#include "Vector2D.h"
#include "MovingObjectsData.h"
#include "Framework.h"
#include "ScreenSpace.h"
#include "SpritesData.h"

struct AxisValues
{
	bool moveMap = true;
	int posVar = 0;
	int& mapVar;
	FRKey sideToCheck = FRKey::LEFT;
	int centerPos = 0;
	int screenSize = 0;
	float deltaPos = 0.0;
	AxisValues(int posVar, int& mapVar, int screenSize);
};

struct sideValues
{
	std::vector<int> dirs;
	std::vector<AxisValues*> axisValues;
	std::vector<FRKey> sides;
	std::vector<int> screenBoundaries;
	float velocities[4];
	float inertias[4] = { 0.1f, 0.1f, 0.1f, 0.1f };
	bool isSlowingDown[4];
	
};

class Player
{
public:
	Player(ScreenSpace* screenSpace, SpritesData* spritesData);
	Player();
	void ResetValues();
	void move(float);
	int getX();
	int getY();
	void addSpeed(float deltaTime, FRKey key);
	void slowDown(FRKey key);

private:
	AxisValues* xValues = nullptr;
	AxisValues* yValues = nullptr;
	sideValues sideValues;
	float playerSpeed = 300.0f;
	void CheckMapBounds(size_t i);
	void CheckScreenBoundsPos(size_t i);
	void CalculateInertia(size_t i);
	void changePosition(AxisValues* axis, int speed, int sideA, int sideB);
	void CheckScreenBoundsNeg(size_t i);
};