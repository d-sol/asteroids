#include "Player.h"
#include <cmath>

Player::Player(ScreenSpace* screenSpace, SpritesData* spritesData)
{
	int w = spritesData->widths[PLAYER];
	int h = spritesData->heights[PLAYER];
	int widthScreenBoundary = screenSpace->mapWidth - screenSpace->screenWidth;
	int heightScreenBoundary = screenSpace->mapHeight - screenSpace->screenHeight;
	xValues = new AxisValues(screenSpace->screenCenter.x - w / 2, screenSpace->mapOffset.x, screenSpace->screenWidth);
	yValues = new AxisValues(screenSpace->screenCenter.y - h / 2, screenSpace->mapOffset.y, screenSpace->screenHeight);
	sideValues.axisValues = { xValues, xValues, yValues, yValues };
	sideValues.sides = { FRKey::RIGHT, FRKey::LEFT, FRKey::DOWN, FRKey::UP };
	sideValues.screenBoundaries = { widthScreenBoundary , 0, heightScreenBoundary, 0 };
	sideValues.dirs = { 1, -1, 1, -1 };
}
AxisValues::AxisValues(int posVar, int& mapVar, int screenSize): posVar(posVar), mapVar(mapVar), centerPos(posVar), screenSize(screenSize)
{};

Player::Player() {};

int Player::getX()
{
	return xValues->posVar;
}

int Player::getY()
{
	return yValues->posVar;
}


void Player::move(float deltaTime)
{
	for (size_t i = 0; i < 4; i++)
	{
		CalculateInertia(i);
	}
	xValues->deltaPos = sideValues.velocities[1] - sideValues.velocities[0];
	yValues->deltaPos = sideValues.velocities[3] - sideValues.velocities[2];
	auto magnitude = sqrtf(xValues->deltaPos * xValues->deltaPos + yValues->deltaPos * yValues->deltaPos);
	if (magnitude > 0)
	{
		clampLenFloat(xValues->deltaPos, yValues->deltaPos, playerSpeed);
		xValues->deltaPos *= deltaTime;
		yValues->deltaPos *= deltaTime;
		changePosition(xValues, xValues->deltaPos, static_cast<int>(FRKey::RIGHT), static_cast<int>(FRKey::LEFT));
		changePosition(yValues, yValues->deltaPos, static_cast<int>(FRKey::DOWN), static_cast<int>(FRKey::UP));
	}	
}

void Player::changePosition(AxisValues* axis, int speed, int sideA, int sideB)
{
	if (!axis->moveMap)
	{
		axis->posVar -= speed;
		CheckScreenBoundsPos(sideA);
		CheckScreenBoundsNeg(sideB);
	}
	else
	{
		CheckMapBounds(sideA);
		CheckMapBounds(sideB);
		if (axis->moveMap)
			axis->mapVar += speed;
	}
}

void Player::CalculateInertia(size_t i)
{
	if (sideValues.isSlowingDown[i])
	{
		sideValues.velocities[i] = round(sideValues.velocities[i] - sideValues.inertias[i]);
		sideValues.inertias[i] *= 1.15f;
		if (sideValues.velocities[i] < 0.001) {
			sideValues.isSlowingDown[i] = false;
			sideValues.velocities[i] = 0;
			sideValues.inertias[i] = 0.1f;
		}
	}
}

void Player::CheckMapBounds(size_t i)
{
	AxisValues* axisValues = sideValues.axisValues[i];
	int varToCheck = axisValues->mapVar;
	if (sideValues.sides[i] == FRKey::RIGHT || sideValues.sides[i] == FRKey::DOWN) varToCheck = abs(varToCheck);
	if (varToCheck > sideValues.screenBoundaries[i] + sideValues.dirs[i] * axisValues->deltaPos)
	{
		axisValues->mapVar = -(sideValues.screenBoundaries[i]);
		axisValues->sideToCheck = sideValues.sides[i];
		axisValues->moveMap = false;
	}
}

void Player::CheckScreenBoundsPos(size_t i)
{
	AxisValues* axisValues = sideValues.axisValues[i];
	if (axisValues->sideToCheck == sideValues.sides[i] && axisValues->posVar < axisValues->centerPos)
	{
		axisValues->moveMap = true;
	}
	if (axisValues->posVar < 0)
	{
		axisValues->mapVar = -(sideValues.screenBoundaries[i]);
		axisValues->posVar = axisValues->screenSize;
		axisValues->sideToCheck = sideValues.sides[i];
	}
}

void Player::CheckScreenBoundsNeg(size_t i)
{
	AxisValues* axisValues = sideValues.axisValues[i];
	if (axisValues->sideToCheck == sideValues.sides[i] && axisValues->posVar > axisValues->centerPos)
	{
		axisValues->moveMap = true;
	}
	if (axisValues->posVar > axisValues->screenSize)
	{
		axisValues->mapVar = 0;
		axisValues->posVar = 0;
		axisValues->sideToCheck = sideValues.sides[i];
	}
}

void Player::addSpeed(float deltaTime, FRKey key)
{
	sideValues.velocities[static_cast<int>(key)] = playerSpeed;
};

void Player::slowDown(FRKey key)
{
	sideValues.isSlowingDown[static_cast<int>(key)] = true;
}

void Player::ResetValues()
{
	xValues->posVar = xValues->centerPos;
	yValues->posVar = yValues->centerPos;
	xValues->deltaPos = 0;
	yValues->deltaPos = 0;
	for (int i = 0; i < 4; i++)
	{
		sideValues.velocities[i] = 0;
		sideValues.inertias[i] = 0.1f;
		sideValues.isSlowingDown[i] = false;
	}

}