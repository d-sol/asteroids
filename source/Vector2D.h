#pragma once
struct Vector2D
{
	int x;
	int y;
	Vector2D(int x, int y);
	Vector2D();
	const bool InRange(int num);
	float getLength();
	void clampToLength(float getLength);
private:
	float length;
};

void clampLenFloat(float& x, float& y, float len);