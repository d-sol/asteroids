#pragma once
#include "Vector2D.h"
#include "SpritesData.h"
#include "Player.h"
#include "CollisionHandler.h"

class Grid
{
public:

	Grid(MovingObjects* data, int canvasW, int canvasH, SpritesData* spritesData, CollisionHandler* collHandler, short asteroidsNum);
	void reset();
	void registerPositions();
	void resolveCollisions();
	bool getPlayerCollisions(Player player, Vector2D mapOffset);

private:
	SpritesData* spritesData = nullptr;
	MovingObjects* data = nullptr;
	CollisionHandler* collHandler = nullptr;
	unsigned int colNum;
	unsigned int rowNum;
	int cellPx;
	
	typedef std::vector<size_t> Cell;

	struct GridCellsRange
	{
		int colStart;
		int rowStart;
		int colEnd;
		int rowEnd;
	};
	std::vector<Cell> _cells;
	std::vector<GridCellsRange> cellsRanges;
	GridCellsRange CellsRange(int x, int y, int w, int h);
};