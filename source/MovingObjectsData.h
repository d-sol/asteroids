#pragma once
#include "Vector2D.h"
#include <vector>
#include <set>
#include "SpritesData.h"
#include "ScreenSpace.h"

class MovingObjects
{
public:
	std::vector<OBJECT_TYPE> types;
	std::vector<int> x;
	std::vector<int> y;
	std::vector<float> dx;
	std::vector<float> dy;
	void addBullet(int playerX, int playerY, Vector2D mousePos);
	void addAsteroid(Vector2D player, bool isBig);
	void destroyObjects();
	unsigned int aliveAsteroids = 0;
	void clearAllData();
	float getAsteroidsVelocity();
	MovingObjects(SpritesData* spritesData, ScreenSpace* screenSpace, int ammo, short asteroidsNum);
	void move(float deltaTime);
	void markDead(size_t ind);

private:
	std::set<size_t> deathSet;
	SpritesData* spritesData;
	ScreenSpace* screenSpace;
	int aliveBullets = 0;
	void addMovingObjectData(Vector2D start, Vector2D movementVector);
	int indexingOffset_ = 0;
	void destroyObject(int);
	Vector2D generateRandomVelocity();
	float asteroidsVelocity = 100.0f;
	float bulletsVelocity = 400.0f;
	float vectorMultiplier = 1.0f;
	float magnitude = 1;
	int ammo;
};

