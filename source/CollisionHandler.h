#pragma once
#include "MovingObjectsData.h"
#include "SpritesData.h"


class CollisionHandler
{
public: 
	const void resolveCollision(size_t indA, size_t indB);
	CollisionHandler(MovingObjects* data, SpritesData* spritesData);
	const bool boundsOverlap(Vector2D posA, Vector2D posB, OBJECT_TYPE typeA, OBJECT_TYPE typeB);

private:
	MovingObjects* data;
	SpritesData* spritesData;
	OBJECT_TYPE typeA = SMALL_ASTEROID;
	OBJECT_TYPE typeB = SMALL_ASTEROID;
	Vector2D collision;
	float overlap = 0.0f;
	float asteroidsVelocity;
	void resolveAsteroidBulletCollision(size_t asteroidInd, size_t bulletInd);
	void resolveAsteroidAsteroidCollision(size_t indA, size_t indB);
	void addCollisionVelocity(Vector2D Collision, size_t indA, size_t indB);
	void setNewVelocities(float x, float y, size_t ind);
};
