#include "Vector2D.h"
#include <cmath>

Vector2D::Vector2D()
{
	x = 0;
	y = 0;
	length = 0;
}

const bool Vector2D::InRange(int num)
{
	return num >= x && num <= y;
}

Vector2D::Vector2D(int x, int y) : x(x), y(y) 
{
	length = sqrtf(x * x + y * y);
};

float Vector2D::getLength()
{
	return length;
};

void Vector2D::clampToLength(float getLength)
{
	float multiplier = getLength / this->getLength();
	x = round(x * multiplier);
	y = round(y * multiplier);
}

void clampLenFloat(float& x, float& y, float len)
{
	float multiplier = len / sqrtf(x * x + y * y);
	x = x * multiplier;
	y = y * multiplier;
}