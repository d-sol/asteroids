#include "SpritesData.h"


SpritesData::SpritesData()
{
	int spriteW, spriteH;
	const char paths[spriteNum][30] =
	{
		"data\\small_asteroid.png",
		"data\\big_asteroid.png",
		"data\\spaceship.png",
		"data\\bullet.png",
		"data\\reticle.png",
		"data\\background.png"
	};
	for (size_t i = 0; i < spriteNum; i++)
	{
		Sprite* spr = createSprite(paths[i]);
		sprites.push_back(spr);
		getSpriteSize(spr, spriteW, spriteH);
		widths.push_back(spriteW);
		heights.push_back(spriteH);
	}
}


SpritesData::~SpritesData()
{
	for (int i = 0; i < spriteNum; i++)
	{
		destroySprite(sprites[i]);
	}
}
