#include "Framework.h"
#include <vector>
#include "MovingObjectsData.h"
#include "Grid.h"
#include <algorithm>

Grid::Grid(MovingObjects* data, int canvasW, int canvasH, SpritesData* spritesData, CollisionHandler* collHandler, short asteroidsNum) :
	data(data), spritesData(spritesData), collHandler(collHandler)
{
	cellPx = canvasW/asteroidsNum;
	if (cellPx < 15) cellPx = 15;
	if (cellPx > 50) cellPx = 50;
	colNum = ceil(static_cast<float>(canvasW) / cellPx) + 1;
	rowNum = ceil(static_cast<float>(canvasH) / cellPx) + 1;
	for (size_t i = 0; i < rowNum * colNum; ++i)
	{
		_cells.emplace_back();
	}
}

void Grid::reset()
{
	for (auto& cell : _cells)
	{
		cell.clear();
	}
	cellsRanges.clear();
}

void Grid::registerPositions()
{
	for (size_t i = 0; i < data->x.size(); ++i)
	{
		int w = spritesData->widths[data->types[i]];
		int h = spritesData->heights[data->types[i]];
		int x = data->x[i];
		int y = data->y[i];
		GridCellsRange cellsRange = CellsRange(x, y, w, h);
		cellsRanges.push_back(cellsRange);

		for (int c = cellsRange.colStart; c <= cellsRange.colEnd; ++c)
		{
			for (int r = cellsRange.rowStart; r <= cellsRange.rowEnd; ++r)
			{
				Cell& cell{ _cells[r * colNum + c] };
				cell.push_back(i);
			}
		}
	}
}

Grid::GridCellsRange Grid::CellsRange(int x, int y, int w, int h)
{
	Vector2D topLeft(x, y);
	Vector2D bottomRight(x + w, y + h);
	GridCellsRange range;

	range.colStart = static_cast<int>(topLeft.x / cellPx);
	if (range.colStart < 0)
		range.colStart = 0;

	range.rowStart = static_cast<int>(topLeft.y / cellPx);
	if (range.rowStart < 0)
		range.rowStart = 0;

	range.colEnd = static_cast<int>(bottomRight.x / cellPx);
	if (range.colEnd >= static_cast<int>(colNum))
		range.colEnd = colNum - 1;

	range.rowEnd = static_cast<int>(bottomRight.y / cellPx);
	if (range.rowEnd >= static_cast<int>(rowNum))
		range.rowEnd = rowNum - 1;
	return range;
}



void Grid::resolveCollisions()
{
	registerPositions();
	for (size_t c = 0; c < _cells.size(); ++c)
	{
		Cell const& cell =  _cells[c];

		int const currRow = static_cast<int>(c / colNum);
		int const currCol = static_cast<int>(c - currRow * colNum);

		for (int i = 0; i < static_cast<int>(cell.size()) - 1; ++i)
		{
			size_t indA = cell[i];
			GridCellsRange const& rangeA = cellsRanges[indA];

			for (int j = i + 1; j < cell.size(); ++j)
			{
				size_t indB =  cell[j];
				GridCellsRange const& rangeB = cellsRanges[indB];

				bool shouldCheck{ (rangeA.rowStart == currRow || rangeB.rowStart == currRow) &&
				(rangeA.colStart == currCol || rangeB.colStart == currCol) };

				if (shouldCheck && collHandler->boundsOverlap(Vector2D(data->x[indA], data->y[indA]), 
					Vector2D(data->x[indB], data->y[indB]), data->types[indA], data->types[indB]))
				{
					collHandler->resolveCollision(indA, indB);
				}
			}
		}
	}
}
	
bool Grid::getPlayerCollisions(Player player, Vector2D mapOffset)
{
	int w = spritesData->widths[PLAYER];
	int h = spritesData->heights[PLAYER];
	int x = player.getX() - mapOffset.x;
	int y = player.getY() - mapOffset.y;
	GridCellsRange cellsRange{ CellsRange(x, y, w, h) };
	
	for (size_t curRow = cellsRange.rowStart; curRow < cellsRange.rowEnd; curRow++)
	{
		for (size_t curCol = cellsRange.colStart; curCol < cellsRange.colEnd; curCol++)
		{
			for (auto objectInd : _cells[colNum * curRow + curCol])
			{
				if (data->types[objectInd] != BULLET && 
					collHandler->boundsOverlap(Vector2D(x, y), Vector2D(data->x[objectInd], data->y[objectInd]), PLAYER, data->types[objectInd]))
				return true;
			}
		}
	}
	return false;
}
