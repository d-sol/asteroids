#pragma once
#include "Vector2D.h"
#include <vector>
#include "Framework.h"

enum OBJECT_TYPE {
	SMALL_ASTEROID,
	BIG_ASTEROID,
	PLAYER,
	BULLET
};

struct SpritesData
{
	std::vector<Sprite*> sprites;
	std::vector<int> widths;
	std::vector<int> heights;
	static const int spriteNum = 6;
	SpritesData();
	~SpritesData();
};
