#include "Framework.h"
#include <vector>
#include <time.h>
#include "MovingObjectsData.h"
#include "Grid.h"
#include <set>
#include <algorithm>
#include "SpritesData.h"
#include "Player.h"
#include <string>
#include <iostream>

constexpr auto FPS = 60;

class MyFramework : public Framework
{
public:

	virtual void PreInit(int& width, int& height, bool& fullscreen)
	{
		auto screenSpace = this->screenSpace;
		width = this->screenSpace->screenWidth;
		height = this->screenSpace->screenHeight;
		fullscreen = false;
		srand(time(0));
	}

	virtual bool Init() {
		prevTick = getTickCount();
		showCursor(false);
		spritesData = new SpritesData();
		movingObjects = new MovingObjects(spritesData, screenSpace, ammo, asteroidsNum);
		collHandler = new CollisionHandler(movingObjects, spritesData);
		player = new Player(screenSpace, spritesData);
		grid = new Grid(movingObjects, screenSpace->mapWidth, screenSpace->mapHeight, spritesData, collHandler, asteroidsNum);
		backgroundW = spritesData->widths[5];
		backgroundH = spritesData->heights[5];
		return true;
	}

	virtual void Close() 
	{
		delete spritesData;
		delete movingObjects;
		delete screenSpace;
		delete player;
		delete collHandler;
		delete grid;
	}

	virtual bool Tick() 
	{
		frameRateHighEnough = updateTimer();
		drawBackground();
		drawReticle();
		if (millisSinceRestart < restartLimit)
		{
			restarting = true;
			drawPlayerFlickering();
			if (millisSinceRestart < restartLimit2)
			{
				return false;
			}
		}
		else
		{
			restarting = false;
			drawPlayerAlive();
		}
		drawObjects();
		if (frameRateHighEnough)
		{
			if (movingObjects->aliveAsteroids < asteroidsNum)
			{
				addAsteroid();
			}
			move();
			grid->reset();
			grid->resolveCollisions();
			if (!restarting && grid->getPlayerCollisions(*player, screenSpace->mapOffset))
			{
				Restart();
			}
			movingObjects->destroyObjects();
		}
		return false;
	}

	virtual void onMouseMove(int x, int y, int xrelative, int yrelative) 
	{
		mousePos.x = x;
		mousePos.y = y;
	}

	virtual void onMouseButtonClick(FRMouseButton button, bool isReleased)
	{
		if (button == FRMouseButton::LEFT && !restarting && curTick - lastMouseClick > clickLimit) 
		{
			movingObjects->addBullet(player->getX(), player->getY(), mousePos);
			lastMouseClick = curTick;
		}
	}

	virtual void onKeyPressed(FRKey k) 
	{
		player->addSpeed(deltaTime, k);
	}

	virtual void onKeyReleased(FRKey k) 
	{
		player->slowDown(k);
	}

	virtual const char* GetTitle() override
	{
		return "asteroids";
	}

	MyFramework(int screenW, int screenH, int mapW, int mapH, int asteroids, int ammo): Framework(), asteroidsNum(asteroids), ammo(ammo)
	{
		if (mapW < screenW)
		{
			mapW = screenW;
		}
		if (mapH < screenH)
		{
			mapH = screenH;
		}
		if (screenW < 300)
		{
			screenW = 300;
		}
		if (screenH < 300)
		{
			screenH = 300;
		}
		screenSpace = new ScreenSpace(mapW, mapH, screenW, screenH);
	}

private:

	SpritesData* spritesData = nullptr;
	ScreenSpace* screenSpace = nullptr;
	CollisionHandler* collHandler = nullptr;
	const unsigned short asteroidsNum;
	const unsigned short ammo;
	unsigned int curTick = 0;
	Vector2D mousePos;
	bool restarting = false;
	unsigned int millisSinceRestart = 0;
	Player* player = nullptr;
	Grid* grid = nullptr;
	MovingObjects* movingObjects = nullptr;
	bool frameRateHighEnough = true;
	float deltaTime = 0;
	unsigned int prevTick = 0;
	unsigned int lastMouseClick = 0;
	unsigned int restartTick = 0;
	unsigned int backgroundW  = 0;
	unsigned int backgroundH = 0;
	unsigned int clickLimit = 400;
	unsigned int restartLimit = 5000;
	unsigned int restartLimit2 = 1500;
	const short int treshold = 15s0;

	void drawBackground()
	{
		for (int accW = 0; accW < screenSpace->mapWidth; accW += backgroundW)
		{
			for (int accH = 0; accH < screenSpace->mapHeight; accH += backgroundH)
			{
				drawSprite(spritesData->sprites[5], screenSpace->mapOffset.x + accW, screenSpace->mapOffset.y + accH);
			}
		}
	}

	void drawPlayerFlickering()
	{
		if ((curTick - restartTick) % 600 < 300) drawPlayerAlive();
	}

	void drawPlayerAlive()
	{
		drawSprite(spritesData->sprites[PLAYER], player->getX(), player->getY());
	}

	void drawReticle()
	{
		drawSprite(spritesData->sprites[4], mousePos.x - spritesData->widths[4] / 2, mousePos.y - spritesData->heights[4] / 2);
	}

	void drawObjects()
	{
		for (size_t i = 0; i < movingObjects->x.size(); i++)
		{
			drawSprite(spritesData->sprites[movingObjects->types[i]], movingObjects->x[i] + screenSpace->mapOffset.x, movingObjects->y[i] + screenSpace->mapOffset.y);
		}
	}

	void addAsteroid()
	{
		Vector2D tabuZoneX(player->getX() - treshold, player->getX() + treshold);
		Vector2D tabuZoneY(player->getY() - treshold, player->getY() + treshold);
		Vector2D start(rand() % screenSpace->mapWidth - 10, rand() % screenSpace->mapHeight - 10);
		int type = rand() % 2;
		int w = spritesData->widths[type]/2;
		int h = spritesData->heights[type]/2;
		while ((tabuZoneX.InRange(start.x - w) || tabuZoneX.InRange(start.x + w)) 
			&& tabuZoneY.InRange(start.y - h) || tabuZoneY.InRange(start.y + h))
		{																
			start.x = rand() / screenSpace->mapWidth - 10;
			start.y = rand() / screenSpace->mapWidth - 10;
		}
		movingObjects->addAsteroid(start, type);
	}

	void move()
	{
		player->move(deltaTime);
		movingObjects->move(deltaTime);
	}

	bool updateTimer()
	{
		curTick = getTickCount();
		millisSinceRestart = curTick - restartTick;
		deltaTime = (curTick - prevTick)*0.001f;
		if (deltaTime < 1.0f/FPS)
		{
			return false;
		}
		else {
			prevTick = curTick;
			return true;
		}
	}

	void Restart()
	{
		grid->reset();
		restartTick = curTick;
		restarting = true;
		curTick = 0;
		movingObjects->destroyObjects();
		screenSpace->mapOffset = screenSpace->mapStartingOffset;
		player->ResetValues();
		movingObjects->clearAllData();
	}
};

int main(int argc, char* argv[])
{
	int windowW = 204 * 4, windowH = 140 * 4;
	int mapW = 204*6, mapH = 140*6;
	int num_asteroids = 15;
	int num_ammo = 5;
	for (size_t i = 1; i < argc; i++) 
	{
		if (std::string(argv[i]) == "-window") 
		{
			i++;
			std::string window = std::string(argv[i]);
			windowW = stoi(window.substr(0, window.find("x")));
			windowH = stoi(window.substr(window.find("x") + 1, window.size()));
			continue;
		}
		if (std::string(argv[i]) == "-map") 
		{
			i++;
			std::string map = std::string(argv[i]);
			mapW = stoi(map.substr(0, map.find("x")));
			mapH = stoi(map.substr(map.find("x") + 1, map.size()));
			continue;
		}
		if (std::string(argv[i]) == "-num_enemies") 
		{
			i++;
			num_asteroids = std::stoi(argv[i]);
			continue;
		}
		if (std::string(argv[i]) == "-num_ammo") 
		{
			i++;
			num_ammo = std::stoi(argv[i]);
			continue;
		}
	}
	
	return run(new MyFramework(windowW, windowH, mapW, mapH, num_asteroids, num_ammo));
};

