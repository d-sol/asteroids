#pragma once
#include "Vector2D.h"

struct ScreenSpace
{
	Vector2D mapOffset;
	int mapWidth;
	int mapHeight;

	int screenWidth;
	int screenHeight;
	Vector2D screenCenter;
	Vector2D mapStartingOffset;
	ScreenSpace(int mapWidth,
		int mapHeight,
		int screenWidth,
		int screenHeight);
};