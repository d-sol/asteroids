#include "ScreenSpace.h"

ScreenSpace::ScreenSpace(int mapWidth, int mapHeight, int screenWidth, int screenHeight) : 
	mapWidth(mapWidth), mapHeight(mapHeight), screenWidth(screenWidth), screenHeight(screenHeight) 
{
	screenCenter = Vector2D(screenWidth / 2, screenHeight / 2);
	mapOffset = Vector2D(screenCenter.x - mapWidth / 2, screenCenter.y - mapHeight / 2);
	mapStartingOffset = mapOffset;
}