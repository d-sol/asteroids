#include "Framework.h"
#include <vector>
#include "MovingObjectsData.h"
#include <algorithm>

MovingObjects::MovingObjects(SpritesData* spritesData, ScreenSpace* screenSpace, int ammo, short asteroidsNum) :
	ammo(ammo), spritesData(spritesData), screenSpace(screenSpace)
{
	int sum = (ammo + asteroidsNum * 1.6f);
	types.reserve(sum);
	x.reserve(sum);
	y.reserve(sum);
	dx.reserve(sum);
	dy.reserve(sum);
}

void MovingObjects::clearAllData()
{
	types.clear();
	x.clear();
	y.clear();
	dx.clear();
	dy.clear();

	aliveBullets = 0;
	aliveAsteroids = 0;
}

void MovingObjects::addBullet(int playerX, int playerY, Vector2D mouse)
{
	Vector2D start(playerX + spritesData->widths[BULLET] / 2 - screenSpace->mapOffset.x,
		playerY + spritesData->heights[BULLET] / 2 - screenSpace->mapOffset.y);
	Vector2D movementVector((mouse.x - start.x - screenSpace->mapOffset.x), (mouse.y - start.y - screenSpace->mapOffset.y));
	aliveBullets++;
	if (aliveBullets > ammo)
	{
		deathSet.insert(find(types.begin(), types.end(), BULLET) - types.begin()); //find the oldest bullet 
	}
	vectorMultiplier = bulletsVelocity;
	types.push_back(BULLET);
	addMovingObjectData(start, movementVector);
}

void  MovingObjects::addAsteroid(Vector2D start, bool isBig)
{
	aliveAsteroids++;
	vectorMultiplier = asteroidsVelocity;
	types.push_back(static_cast<OBJECT_TYPE>(isBig));
	addMovingObjectData(start, generateRandomVelocity());
}

Vector2D MovingObjects::generateRandomVelocity()
{
	Vector2D velocity(rand() % 50 + 1, rand() % 50 + 1);
	if (rand() % 2 == 0) velocity.x *= -1;
	if (rand() % 2 == 0) velocity.y *= -1;
	return velocity;
}

void MovingObjects::markDead(size_t ind)
{
	deathSet.insert(ind);
}

void MovingObjects::addMovingObjectData(Vector2D start, Vector2D movementVector)
{
	this->x.push_back(start.x);
	this->y.push_back(start.y);
	movementVector.clampToLength(vectorMultiplier);
	dx.push_back(round(movementVector.x));
	dy.push_back(round(movementVector.y));
}

void MovingObjects::destroyObject(int ind)
{
	x.erase(x.begin() + ind);
	y.erase(y.begin() + ind);
	dx.erase(dx.begin() + ind);
	dy.erase(dy.begin() + ind);
	int type = types[ind];
	if (type == SMALL_ASTEROID || type == BIG_ASTEROID) aliveAsteroids--;
	if (type == BULLET) aliveBullets--;
	types.erase(types.begin() + ind);
}

void MovingObjects::destroyObjects()
{
	for (auto ind : deathSet)
	{
		destroyObject(ind - indexingOffset_);
		indexingOffset_++;
	}
	indexingOffset_ = 0;
	deathSet.clear();
}

float MovingObjects::getAsteroidsVelocity()
{
	return asteroidsVelocity;
}



void MovingObjects::move(float deltaTime)
{
	for (size_t i = 0; i < x.size(); i++)
	{
		int w = spritesData->widths[types[i]];
		int h = spritesData->heights[types[i]];
		if (y[i] > screenSpace->mapHeight)
		{
			y[i] = -h;
		}
		else if (y[i] < -h)
		{
			y[i] = screenSpace->mapHeight;
		}
		if (x[i] < -w)
		{
			x[i] = screenSpace->mapWidth;
		}
		else if (x[i] > screenSpace->mapWidth)
		{
			x[i] = -w;
		}
		x[i] += round(dx[i] * deltaTime);
		y[i] += round(dy[i] * deltaTime);
	}
}

