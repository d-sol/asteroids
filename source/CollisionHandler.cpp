#include "CollisionHandler.h"

CollisionHandler::CollisionHandler(MovingObjects* data, SpritesData* spritesData) :data(data), spritesData(spritesData)
{
	asteroidsVelocity = data->getAsteroidsVelocity();
}

const void CollisionHandler::resolveCollision(size_t indA, size_t indB)
{
	if ((typeA == SMALL_ASTEROID || typeA == BIG_ASTEROID) && (typeB == SMALL_ASTEROID || typeB == BIG_ASTEROID))
	{
		resolveAsteroidAsteroidCollision(indA, indB);
	}
	else
	{
		if (typeA == BULLET)
		{
			resolveAsteroidBulletCollision(indB, indA);
		}
		else
		{
			resolveAsteroidBulletCollision(indA, indB);
		}
	}
}

void CollisionHandler::resolveAsteroidBulletCollision(size_t asteroidInd, size_t bulletInd)
{
	if (data->types[asteroidInd] == BIG_ASTEROID)
	{
		Vector2D start(data->x[asteroidInd], data->y[asteroidInd] - 30);
		Vector2D startB(data->x[asteroidInd], data->y[asteroidInd] + 30);
		data->addAsteroid(start, false);
		data->addAsteroid(startB, false);
	}
	data->markDead(asteroidInd);
	data->markDead(bulletInd);
}

const bool CollisionHandler::boundsOverlap(Vector2D posA, Vector2D posB, OBJECT_TYPE typeA, OBJECT_TYPE typeB)
{
	int wA = spritesData->widths[typeA];
	int hA = spritesData->heights[typeA];
	int xCentA = posA.x + wA / 2;
	int yCentA = posA.y + hA / 2;

	int wB = spritesData->widths[typeB];
	int hB = spritesData->heights[typeB];
	int xCentB = posB.x + wA / 2;
	int yCentB = posB.y + hA / 2;
	collision = Vector2D(xCentA - xCentB, yCentA - yCentB);
	overlap = ((hA / 2) + (hB / 2) + 20 - collision.getLength());
	if (overlap > 0)
	{
		this->typeA = typeA;
		this->typeB = typeB;
		return true;
	};
	return false;
}

void CollisionHandler::resolveAsteroidAsteroidCollision(size_t indA, size_t indB)
{
	collision.clampToLength((overlap + 10) / 2); // draw asteroids apart to ensure that no overlap is visible
	data->x[indB] -= collision.x / 2;
	data->y[indB] -= collision.y / 2;
	data->x[indA] += collision.x / 2;
	data->y[indA] += collision.y / 2;
	addCollisionVelocity(collision, indA, indB);
}

// solve for the new velocities using the 1-dimensional elastic collision equations, masses are the same
void CollisionHandler::addCollisionVelocity(Vector2D collision, size_t indA, size_t indB)
{
	float distance = collision.getLength();
	float collisionX = (float)collision.x / distance;
	float collisionY = (float)collision.y / distance;
	float aci = data->dx[indA] * collisionX + data->dy[indA] * collisionY;
	float bci = data->dx[indB] * collisionX + data->dy[indB] * collisionY;
	float acf = bci;
	float bcf = aci;
	setNewVelocities((acf - aci) * collisionX, (acf - aci) * collisionY, indA);
	setNewVelocities((bcf - bci) * collisionX, (bcf - bci) * collisionY, indB);
}

void CollisionHandler::setNewVelocities(float x, float y, size_t ind)
{
	if (x == 0 && y == 0) x = 1;
	clampLenFloat(x, y, asteroidsVelocity);
	data->dx[ind] = x;
	data->dy[ind] = y;
}
